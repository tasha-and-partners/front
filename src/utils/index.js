export const chunk = (arr, size) => (
  Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
    arr.slice(i * size, i * size + size)
  )
)

export const getErrorMsgFromResponse = err => {
  if (err.response && err.response.data && err.response.data.message) {
    return err.response.data.message
  }

  return err.message
}
