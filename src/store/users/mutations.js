export const setUsers = (state, users) => {
  state.users = [...users]
}

export const setUser = (state, user) => {
  state.user = { ...user }
}

export const setKeynoteSpeakers = (state, keynoteSpeakers) => {
  state.keynoteSpeakers = [...keynoteSpeakers]
}

export const setKeynoteSpeaker = (state, keynoteSpeaker) => {
  state.keynoteSpeaker = { ...keynoteSpeaker }
}
