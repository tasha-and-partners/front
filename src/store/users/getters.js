export const users = state => state.users

export const user = state => state.user

export const keynoteSpeakers = state => state.keynoteSpeakers

export const keynoteSpeaker = state => state.keynoteSpeaker
