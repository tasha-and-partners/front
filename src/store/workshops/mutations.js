export const setWorkshops = (state, workshops) => {
  state.workshops = [...workshops]
}

export const setWorkshop = (state, workshop) => {
  state.workshop = { ...workshop }
}
