export const setTopics = (state, topics) => {
  state.topics = [...topics]
}

export const setTopic = (state, topic) => {
  state.topic = { ...topic }
}

export const setPreferred = (state, preferred) => {
  state.preferred = [...preferred]
}
