import { api } from 'boot/axios'

const endpoint = '/users'

export const getUsers = params => {
  return api.get(endpoint, { params })
}

export const getKeynoteSpeakers = () => {
  return api.get('keynote-speakers')
}

export const getUser = id => {
  return api.get(`${endpoint}/${id}`)
}

export const createUser = data => {
  return api.post(`${endpoint}`, data)
}

export const updateUser = (id, data) => {
  return api.put(`${endpoint}/${id}`, data)
}

export const updateUserMultipart = (id, data) => {
  return api.post(`${endpoint}/${id}`, data)
}

export const deleteUser = id => {
  return api.delete(`${endpoint}/${id}`)
}
