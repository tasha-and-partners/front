import { api } from 'boot/axios'

const endpoint = '/topics'

export const getTopics = params => {
  return api.get(`${endpoint}`, { params })
}

export const getPreferredTopics = () => {
  return api.get('/preferred-topics')
}

export const getTopic = id => {
  return api.get(`${endpoint}/${id}`)
}

export const createTopic = data => {
  return api.post(`${endpoint}`, data)
}

export const updateTopic = (id, data) => {
  return api.put(`${endpoint}/${id}`, data)
}

export const deleteTopic = id => {
  return api.delete(`${endpoint}/${id}`)
}

export const setPreferredTopics = data => {
  return api.post('/preferred-topics', data)
}
