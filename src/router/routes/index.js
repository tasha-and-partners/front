// Routes
import authRoutes from './auth'
// import profileRoutes from './profile'
import siteRoutes from './site'

const routes = [
  ...authRoutes,
  // ...profileRoutes,
  ...siteRoutes,

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    name: 'page404',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
