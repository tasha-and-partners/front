const siteRoutes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'Home', component: () => import('pages/Site/Index.vue') },
      { path: 'profile', name: 'Profile', component: () => import('pages/Profile/Index.vue') }
    ]
  }
]

export default siteRoutes
