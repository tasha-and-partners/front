import { Plugins } from '@capacitor/core'

const { Storage } = Plugins

const build = async to => {
  const USER_KEY = process.env.STORAGE_USER_KEY

  const userObject = await Storage.get({ key: USER_KEY })
  const LOCAL_STORAGE_USER = userObject.value

  const is404 = to.matched.some(record => (record.name === 'page404'))

  // Redirect unauthenticated users to login page when accessing profile route
  if (
    !LOCAL_STORAGE_USER
    && !to.matched.some(record => record.meta.requiresGuest)
    && !is404
    ) {
    return {
      name: 'Login',
      query: { redirect: to.fullPath }
    }
  }

  // Check if route requires to be logged out
  if (to.matched.some(record => record.meta.requiresGuest) && LOCAL_STORAGE_USER) {
    return {
      name: 'Home'
    }
  }

  return null
}

export default { build }
