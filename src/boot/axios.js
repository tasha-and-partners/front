import { Plugins } from '@capacitor/core'
import Vue from 'vue'
import axios from 'axios'

const { Storage } = Plugins

Vue.prototype.$axios = axios

// Create an axios instance specifically for the backend API
const api = axios.create({ baseURL: process.env.API_URL })

api.interceptors.request.use(
  async config => {
    const tokenObject = await Storage.get({ key: process.env.STORAGE_TOKEN_KEY })
    const token = tokenObject.value

    config.headers['Authorization'] = `Bearer ${token}`

    return config
  },

  error => {
    Promise.reject(error)
  }
)

Vue.prototype.$api = api

export { axios, api }
